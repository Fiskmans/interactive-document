﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace InteractiveDocument
{
    abstract class Interactable : Drawable
    {
        public Rectangle boundry;
        abstract public Interactable FindUnderMouse(MouseState ms);

        public Interactable StandardFindUnderMouse(MouseState ms)
        {
            if (boundry.Contains(ms.X, ms.Y))
            {
                return this;
            }
            else return null;
        }

        virtual public void Interact(MouseState ms,MouseState oldms)
        {
            //noop
        }

        virtual public void Update(MouseState ms,MouseState oldms,KeyboardState ks)
        {
            //noop
        }

        virtual public Rectangle GraphicBoundry()
        {
            return boundry;
        }

        virtual public void MoveToTop(Interactable box)
        {
            //Noop
        }
    }
}
