﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Xna.Framework.Graphics;

namespace InteractiveDocument
{
    class lookUpTable
    {
        public static string BoxPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\InteractiveDocument\Infoboxes\";
        public static string PicturePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\InteractiveDocument\Pictures\";
        static Dictionary<string, Texture2D> LoadedTextures;

        public static infoBox Find(string name,Interactable parent)
        {
            Directory.CreateDirectory(BoxPath);
            if (Directory.GetFiles(BoxPath).Contains(BoxPath + name.ToLower() + ".infobox"))
            {
                infoBox temp = new infoBox(Game1.StandardBorder, parent);
                temp.boundry.Size = new Microsoft.Xna.Framework.Point(200, 400);
                temp.SetupFromString(File.ReadAllText(BoxPath + name.ToLower() + ".infobox"));
                return temp;
            }
            // look for infobox file in directory with name <name> read file parse (json) and return result or null
            return null;
        }
        public static Texture2D FindTex(string name, Interactable parent)
        {
            if (LoadedTextures == null)
            {
                LoadedTextures = new Dictionary<string, Texture2D>();
            }
            if (LoadedTextures.ContainsKey(name))
            {
                return LoadedTextures[name];
            }

            Directory.CreateDirectory(PicturePath);
            if (Directory.GetFiles(PicturePath).Contains(PicturePath + name.ToLower() + ".png"))
            {
                using (FileStream FS = new FileStream(PicturePath + name.ToLower() + ".png", FileMode.Open))
                {
                    Texture2D res = Texture2D.FromStream(Game1.PublicDevice, FS);
                    LoadedTextures[name] = res;
                    return res;
                }
            }
            // look for infobox file in directory with name <name> read file parse (json) and return result or null
            return null;
        }
    }
}
