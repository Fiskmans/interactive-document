﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace InteractiveDocument
{
    abstract class Drawable
    {
        public virtual void Draw(SpriteBatch s)
        {
            //noop
        }
    }
}
