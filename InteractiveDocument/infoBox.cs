﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace InteractiveDocument
{
    class infoBox : Interactable
    {
        enum formating{word,picture,animation };
        public List<Component> Content;
        public movingLine line;
        List<formating> Format;
        public List<infoBox> Children;
        public Border border;
        bool Grabbed = false;
        string RawContent;
        

        public infoBox(Border border,Interactable parent)
        {
            //TODO Create from argument
            Content = new List<Component>();
            Format = new List<formating>();
            Children = new List<infoBox>();
            RawContent = "";

            line = new movingLine(parent, this);
            this.border = border;
            Game1.TypedText += new SendText(Type);
        }

        public override void Draw(SpriteBatch s)
        {
            //drawself
            border.Draw(s, boundry, 7, Color.Black);

            line.Draw(s);
            foreach (Component comp in Content)
            {
                comp.Draw(s);
            }

            //draw children
            foreach (infoBox InB in Children)
            {
                InB.Draw(s);
            }
        }

        public override Interactable FindUnderMouse(MouseState ms)
        {
            for (int i = Children.Count-1; i >= 0; i--)
            {
                Interactable res = Children[i].FindUnderMouse(ms);
                if (res != null)
                {
                    return res;
                }
            }
            for (int i = Content.Count - 1; i >= 0; i--)
            {
                Interactable res = Content[i].FindUnderMouse(ms);
                if (res != null)
                {
                    return res;
                }
            }
            return StandardFindUnderMouse(ms);
        }

        public override void Interact(MouseState ms, MouseState oldms)
        {
            if (ms.LeftButton == ButtonState.Pressed && oldms.LeftButton == ButtonState.Released)
            {
                Grabbed = true;
                MoveToTop(null);
            }
            if (ms.RightButton == ButtonState.Pressed && oldms.RightButton == ButtonState.Released)
            {
                boundry.Width = boundry.Width + 50;
            }
        }

        public override void Update(MouseState ms, MouseState oldms, KeyboardState ks)
        {
            foreach (infoBox InB in Children)
            {
                InB.Update(ms, oldms, ks);
            }
            if (Grabbed)
            {
                Move(ms.X - oldms.X, ms.Y - oldms.Y, true);
            }
            if (Grabbed && ms.LeftButton == ButtonState.Released)
            {
                Grabbed = false;
            }
                
        }

        public override void MoveToTop(Interactable box)
        {
            line.parent?.MoveToTop(this);
            if (box != null && (box.GetType() == typeof(infoBox) || typeof(infoBox).IsSubclassOf(box.GetType())))
            {
                for (int i = 0; i < Children.Count - 1; i++)
                {
                    if (Children[i] == box)
                    {
                        Children[i] = Children[i + 1];
                        Children[i + 1] = (infoBox)box;
                    }
                }
            }
        }

        public void Move(int dx,int dy,bool includeChildren)
        {
            boundry.Location = boundry.Location + new Point(dx, dy);

            if (includeChildren)
            {
                foreach (infoBox InB in Children)
                {
                    InB.Move(dx, dy, true);
                }
            }
        }

        public void SetupFromString(string text)
        {
            RawContent = text;
            Content = Helper.BreakdownText(text, this);
            SetupFormat();
        }

        public void SetupFormat()
        {
            int x = 0;
            int y = 0;

            foreach (Component comp in Content)
            {
                int dx = 0;
                int dy = 0;
                #region GetSize
                if (comp.GetType() == typeof(InteractiveWord))
                {
                    InteractiveWord word = (InteractiveWord)comp;
                    word.FillOutBoundry();
                    dx = word.boundry.Width + (int)word.font.MeasureString(" ").X;
                    dy = word.boundry.Height;
                }
                #endregion

                #region Linebreak
                if (x + dx > boundry.Width)
                {
                    x = 0;
                    y += dy;
                }
                #endregion

                comp.boundry.Location = new Point(x, y);
                x += dx;
            }
        }

        public void PruneLostBoxes()
        {
            List<string> words = Helper.FindAllWords(RawContent);
            foreach (infoBox Inb in Children)
            {
                if (words.Contains(((InteractiveWord)Inb.line.parent).text))
                {

                }
            }
        }

        public void Type(string input)
        {
            foreach (char c in input)
            {
                if (c == '\b' && RawContent.Length > 0)
                {
                    RawContent = RawContent.Substring(0, RawContent.Length - 1);
                }
                else
                {
                    RawContent += input;
                }
            }
            Content = Helper.BreakdownText(RawContent,this);
            SetupFormat();
        }
    }
}
