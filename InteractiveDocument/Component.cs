﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace InteractiveDocument
{
    abstract class Component : Interactable
    {
        public infoBox Parent;

        public Component(infoBox Parent)
        {
            this.Parent = Parent;
        }

        public override Interactable FindUnderMouse(MouseState ms)
        {
            if (boundry.Contains(ms.X - Parent.boundry.X, ms.Y - Parent.boundry.Y))
            {
                return this;
            }
            else return null;
        }

        public override Rectangle GraphicBoundry()
        {
            return new Rectangle(boundry.Location + Parent.boundry.Location, boundry.Size);
        }

        public override void MoveToTop(Interactable box)
        {
            Parent?.MoveToTop(this);
        }
    }
}
