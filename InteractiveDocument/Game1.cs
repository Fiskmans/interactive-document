﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System;

namespace InteractiveDocument
{
    public delegate void SendText(string text);

    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        infoBox MainBox;
        MouseState oldms;
        KeyboardState oldks;
        public static Texture2D Pixel;
        public static SpriteFont standardFont;
        public static ContentManager PublicContent;
        public static GraphicsDevice PublicDevice;
        public static event SendText TypedText;
        public static List<Keys> ModifierKeys;

        public static Border StandardBorder;

        public Game1()
        {
            ModifierKeys = new List<Keys>() { Keys.LeftShift, Keys.LeftAlt, Keys.LeftControl, Keys.RightAlt, Keys.RightControl, Keys.RightShift };
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        
        protected override void Initialize()
        {
            base.Initialize();
            IsMouseVisible = true;
            TypedText += new SendText(Helper.Noop);
        }
        protected override void LoadContent()
        {
            oldms = Mouse.GetState();
            oldks = Keyboard.GetState();

            PublicContent = Content;
            PublicDevice = GraphicsDevice;

            spriteBatch = new SpriteBatch(GraphicsDevice);

            //On start Loaded resources
            StandardBorder = new Border(Content.Load<Texture2D>("basicBorder"), 10);
            standardFont = Content.Load<SpriteFont>("standard font");

            //Genererated resources
            Pixel = new Texture2D(GraphicsDevice, 1, 1);
            Pixel.SetData<Color>(new Color[] { Color.White });


            //TODO Load mainbox from file
            MainBox = new infoBox(StandardBorder,null);
            MainBox.boundry = new Rectangle(0, 0, 200, 400);
            MainBox.border = StandardBorder;
            MainBox.SetupFromString("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel erat arcu. Etiam vitae augue eu purus imperdiet elementum. Donec porta ligula mauris, vel tincidunt quam sodales quis. Nunc ultrices orci vel sapien posuere finibus. Cras dignissim cursus convallis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras eget iaculis eros. Ut egestas, velit et hendrerit lobortis, elit metus tincidunt magna, ut porta risus eros eget nunc. Vivamus non mauris luctus, fermentum sem eget, elementum purus. Morbi eget sapien imperdiet, consequat nisl nec, porta orci. Nunc elementum tellus diam, eu gravida purus commodo sit amet.");
            
        }
        protected override void UnloadContent()
        {
        }
        protected override void Update(GameTime gameTime)
        {
            MouseState ms = Mouse.GetState();
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape)) { Exit(); }

            Interactable res = MainBox.FindUnderMouse(ms);
            if (res != null)
            {
                res.Interact(ms, oldms);
            }
            MainBox.Update(ms, oldms, Keyboard.GetState());

            List<Keys> oldkeys = new List<Keys>(oldks.GetPressedKeys());
            string typed = Convert(new List<Keys>(ks.GetPressedKeys()).FindAll(x => ModifierKeys.Contains(x) || !oldkeys.Contains(x)));
            if (typed != "")
            {
                TypedText(typed);
            }

            oldms = ms;
            oldks = ks;
            base.Update(gameTime);
        }

        public string Convert(List<Keys> keys)
        {
            string output = "";
            bool usesShift = (keys.Contains(Keys.LeftShift) || keys.Contains(Keys.RightShift));
 
            foreach (Keys key in keys)
            {
                if (key >= Keys.A && key <= Keys.Z)
                    output += key.ToString();
                else if (key >= Keys.NumPad0 && key <= Keys.NumPad9)
                    output += ((int)(key - Keys.NumPad0)).ToString();
                else if (key >= Keys.D0 && key <= Keys.D9)
                {
                    string num = ((int)(key - Keys.D0)).ToString();
                    #region special num chars
                    if (usesShift)
                    {
                        switch (num)
                        {
                            case "1":
                                {
                                    num = "!";
                                }
                                break;
                            case "2":
                                {
                                    num = "@";
                                }
                                break;
                            case "3":
                                {
                                    num = "#";
                                }
                                break;
                            case "4":
                                {
                                    num = "$";
                                }
                                break;
                            case "5":
                                {
                                    num = "%";
                                }
                                break;
                            case "6":
                                {
                                    num = "^";
                                }
                                break;
                            case "7":
                                {
                                    num = "&";
                                }
                                break;
                            case "8":
                                {
                                    num = "*";
                                }
                                break;
                            case "9":
                                {
                                    num = "(";
                                }
                                break;
                            case "0":
                                {
                                    num = ")";
                                }
                                break;
                            default:
                                //wtf?
                                break;
                        }
                    }
                    #endregion
                    output += num;
                }
                else if (key == Keys.OemPeriod)
                    output += ".";
                else if (key == Keys.OemTilde)
                    output += "'";
                else if (key == Keys.Space)
                    output += " ";
                else if (key == Keys.OemMinus)
                    output += "-";
                else if (key == Keys.OemPlus)
                    output += "+";
                else if (key == Keys.OemQuestion && usesShift)
                    output += "?";
                else if (key == Keys.Back) //backspace
                    output += "\b";
                else if (key == Keys.OemBackslash)
                    output += usesShift ? ">" : "<";
                else if (key == Keys.OemComma)
                    output += usesShift ? ";" : ",";
 
                if (!usesShift) //shouldn't need to upper because it's automagically in upper case
                    output = output.ToLower();
            }
            return output;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();

            Interactable res = MainBox.FindUnderMouse(oldms);
            if (res != null)
            {
                spriteBatch.Draw(Pixel, res.GraphicBoundry(), Color.LimeGreen);
            }

            MainBox.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
