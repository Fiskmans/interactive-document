﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace InteractiveDocument
{
    class movingLine : Drawable
    {
        public Interactable parent;
        public infoBox child;
        Color c = Color.Black;

        public movingLine(Interactable parent,infoBox child)
        {
            this.parent = parent;
            this.child = child;
        }

        public override void Draw(SpriteBatch s)
        {
            if (parent == null || child == null)
            {
                return;
            }
            Point a;
            Point b;
            Helper.FindClosestPoints(parent.GraphicBoundry(), child.GraphicBoundry(), out a, out b);
            s.Draw(Game1.Pixel, new Vector2((a + b).X / 2, (a + b).Y / 2), null, c, (float)Math.Atan2(b.Y - a.Y, b.X - a.X), new Vector2(0.5f), new Vector2((a.ToVector2() - b.ToVector2()).Length(), 1), SpriteEffects.None, 1);
            s.Draw(Game1.Pixel, parent.GraphicBoundry(), new Color(Color.Lime, 0.01f));
            if (parent.GetType() == typeof(InteractiveWord))
            {
                parent.Draw(s);
            }
        }
    }
}
