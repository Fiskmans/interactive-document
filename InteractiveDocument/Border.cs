﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace InteractiveDocument
{
    public class Border
    {
        Texture2D borderTexture;
        int inset;
        public Border(Texture2D borderTexture, int inset)
        {
            this.borderTexture = borderTexture;
            this.inset = inset;
        }
        public void Draw(SpriteBatch s, Rectangle box, int BorderWidth, Color c)
        {
            s.Draw(borderTexture, new Rectangle(box.Left, box.Top, BorderWidth, BorderWidth), new Rectangle(new Point(0), new Point(inset)), c);
            s.Draw(borderTexture, new Rectangle(box.Right - BorderWidth, box.Top, BorderWidth, BorderWidth), new Rectangle(new Point(borderTexture.Width - inset, 0), new Point(inset)), c);
            s.Draw(borderTexture, new Rectangle(box.Left, box.Bottom - BorderWidth, BorderWidth, BorderWidth), new Rectangle(new Point(0, borderTexture.Height - inset), new Point(inset)), c);
            s.Draw(borderTexture, new Rectangle(box.Right - BorderWidth, box.Bottom - BorderWidth, BorderWidth, BorderWidth), new Rectangle(new Point(borderTexture.Width - inset, borderTexture.Width - inset), new Point(inset)), c);

            s.Draw(borderTexture, new Rectangle(box.Left, box.Top + BorderWidth, BorderWidth, box.Height - 2 * BorderWidth), new Rectangle(new Point(0, inset), new Point(inset, borderTexture.Height - inset * 2)), c); //left
            s.Draw(borderTexture, new Rectangle(box.Left + BorderWidth, box.Top, box.Width - 2 * BorderWidth, BorderWidth), new Rectangle(new Point(inset, 0), new Point(borderTexture.Width - inset * 2, inset)), c); // top
            s.Draw(borderTexture, new Rectangle(box.Right - BorderWidth, box.Top + BorderWidth, BorderWidth, box.Height - 2 * BorderWidth), new Rectangle(new Point(borderTexture.Width - inset, inset), new Point(inset, borderTexture.Height - inset * 2)), c); //right
            s.Draw(borderTexture, new Rectangle(box.Left + BorderWidth, box.Bottom - BorderWidth, box.Width - 2 * BorderWidth, BorderWidth), new Rectangle(new Point(inset, borderTexture.Height - inset), new Point(borderTexture.Width - inset * 2, inset)), c); //
        }
    }
}
