﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace InteractiveDocument
{
    class Picture : Component
    {
        Texture2D texture;

        public Picture(infoBox Parent, Texture2D texture) : base(Parent)
        {
            this.texture = texture;
            boundry.Size = new Point(texture.Width, texture.Height);
        }

        public override Interactable FindUnderMouse(MouseState ms)
        {
            return StandardFindUnderMouse(ms);
        }

        public override void Draw(SpriteBatch s)
        {
            s.Draw(texture, GraphicBoundry(), Color.White);
        }
    }
}
