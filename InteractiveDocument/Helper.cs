﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace InteractiveDocument
{
    static class Helper
    {
        public static void FindClosestPoints(Rectangle a,Rectangle b,out Point pointA, out Point pointB)
        {
            if (a.Center.X > b.Center.X)
            {
                if (a.Center.Y > b.Center.Y)
                {
                    pointA = new Point(a.Left, a.Top);
                    pointB = new Point(b.Right, b.Bottom);
                }
                else
                {
                    pointA = new Point(a.Left, a.Bottom);
                    pointB = new Point(b.Right, b.Top);
                }
            }
            else
            {
                if (a.Center.Y > b.Center.Y)
                {
                    pointA = new Point(a.Right, a.Top);
                    pointB = new Point(b.Left, b.Bottom);
                }
                else
                {
                    pointA = new Point(a.Right, a.Bottom);
                    pointB = new Point(b.Left, b.Top);
                }
            }
        }

        public static List<Component> BreakdownText(string text,infoBox ParentBox)
        {
            List<Component> output = new List<Component>();
            foreach (string s in FindAllWords(text))
            {
                if (s.Length > 9 && s[0] == '<')
                {
                    if (s.Substring(1, 7).ToLower() == "texture")
                    {
                        Texture2D res = lookUpTable.FindTex(s.Substring(9, s.Length - 10), ParentBox);
                        if (res != null)
                        {
                            output.Add(new Picture(ParentBox, res));
                            continue;
                        }
                    }
                }

                output.Add(new InteractiveWord(ParentBox, s));
                continue;
            }

            return output;
        }

        public static List<string> FindAllWords(string text)
        {
            List<string> output = new List<string>();

            string temp = "";
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == ' ')
                {
                    output.Add(temp);
                    temp = "";
                }
                else
                {
                    temp += text[i];
                }
            }
            if (temp.Length > 0)
            {
                output.Add(temp);
            }

            return output;
        }

        public static List<InteractiveWord> CastOrDiscard(List<Component> selection)
        {
            List<InteractiveWord> output = new List<InteractiveWord>();
            foreach (Component comp in selection)
            {
                if (comp.GetType() == typeof(InteractiveWord))
                {
                    output.Add((InteractiveWord)comp);
                }
            }
            return output;
        }

        public static bool KeyPressed(KeyboardState ks, KeyboardState oldks,Keys k)
        {
            return ks.IsKeyDown(k) && oldks.IsKeyUp(k);
        }

        public static void Noop(object o)
        {
            //what do you think...
        }
    }
}
