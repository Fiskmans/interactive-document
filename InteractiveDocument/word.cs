﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace InteractiveDocument
{
    class InteractiveWord : Component
    {
        public string text;
        public SpriteFont font = Game1.standardFont;

        public InteractiveWord(infoBox Parent, string text) : base(Parent)
        {
            this.text = text;
            FillOutBoundry();
        }

        public override void Draw(SpriteBatch s)
        {
            s.DrawString(font, text, boundry.Location.ToVector2() + Parent.boundry.Location.ToVector2(), Color.Black);
        }

        public override void Interact(MouseState ms, MouseState oldms)
        {
            if (ms.LeftButton == ButtonState.Pressed && oldms.LeftButton == ButtonState.Released)
            {
                infoBox res = lookUpTable.Find(text, this);
                if (res != null)
                {
                    Parent.Children.Add(res);
                }
            }
        }

        public void FillOutBoundry()
        {
            if (!text.Contains("\n"))
            {
                boundry.Size = font.MeasureString(text).ToPoint();
            }
        }
    }
}
