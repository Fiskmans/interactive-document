﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace InteractiveDocument
{
    class Animation : Component
    {
        public Animation(infoBox Parent) : base(Parent)
        {

        }
        public override Interactable FindUnderMouse(MouseState ms)
        {
            return StandardFindUnderMouse(ms);
        }
    }
}
